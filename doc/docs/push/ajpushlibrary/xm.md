# 小米  

## 开启推送服务  
 
1、登录小米开放平台->点击“管理控制台”->点击“消息推送”,若应用已经存在直接点击“启用推送”即可，若不存在则点击“创建应用”，如下图所示:  
![avatar](../../assets/xm1.png)  
2、创建新应用，输入应用信息，应用包名是唯一的  
![avatar](../../assets/xm2.png)  
3、开启消息推送服务  
![avatar](../../assets/xm3.png)  

## 配置AndroidManifest文件 
 
1、在lib包下面添加jar包，并在app目录下的build.gradle中添加依赖  
![avatar](../../assets/xm4.png)  
2、在AndroidManifest文件中添加权限  
```
    <!-- 这个权限用于进行网络定位 -->
    <uses-permission android:name="android.permission.INTERNET" />

    <!--小米所需要添加的权限-->
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />​
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.READ_PHONE_STATE" />
    <uses-permission android:name="android.permission.VIBRATE" />

    <permission
        android:name="${JPUSH_PKGNAME}.permission.MIPUSH_RECEIVE"
        android:protectionLevel="signature" />
    <uses-permission android:name="${JPUSH_PKGNAME}.permission.MIPUSH_RECEIVE" /><!--这里com.xiaomi.mipushdemo改成app的包名-->
```
3、在AndroidManifest文件中配置推送服务需要的service和receiver 
```
        <service
            android:name="com.xiaomi.push.service.XMJobService"
            android:enabled="true"
            android:exported="false"
            android:permission="android.permission.BIND_JOB_SERVICE"
            android:process=":pushservice" />
        <service
            android:name="com.xiaomi.push.service.XMPushService"
            android:enabled="true"
            android:process=":pushservice" />
        <service
            android:name="com.xiaomi.mipush.sdk.PushMessageHandler"
            android:enabled="true"
            android:exported="true" />
        <service
            android:name="com.xiaomi.mipush.sdk.MessageHandleService"
            android:enabled="true" />

        <receiver
            android:name="com.xiaomi.push.service.receivers.NetworkStatusReceiver"
            android:exported="true">
            <intent-filter>
                <action android:name="android.net.conn.CONNECTIVITY_CHANGE" />

                <category android:name="android.intent.category.DEFAULT" />
            </intent-filter>
        </receiver>
        <receiver
            android:name="com.xiaomi.push.service.receivers.PingReceiver"
            android:exported="false"
            android:process=":pushservice">
            <intent-filter>
                <action android:name="com.xiaomi.push.PING_TIMER" />
            </intent-filter>
        </receiver>
        <receiver
            android:name=".xm.XMPushReceiver"
            android:exported="true">
            <intent-filter>
                <action android:name="com.xiaomi.mipush.RECEIVE_MESSAGE" />
            </intent-filter>
            <intent-filter>
                <action android:name="com.xiaomi.mipush.MESSAGE_ARRIVED" />
            </intent-filter>
            <intent-filter>
                <action android:name="com.xiaomi.mipush.ERROR" />
            </intent-filter>
        </receiver>   
```    
  
## 应用开发  

### 自定义一个BroadcastReceiver类

1、自定义一个BroadcastReceiver类的类名为DemoMessageReceiver。  
2、DemoMessageReceiver类继承PushMessageReceiver类，  
3、实现其中的onReceivePassThroughMessage，onNotificationMessageClicked，onNotificationMessageArrived，onCommandResult和onReceiveRegisterResult方法。
4、onReceivePassThroughMessage：接收服务器发送的透传消息
5、onNotificationMessageClicked：接收服务器发来的通知栏消息（用户点击通知栏时触发）  
6、onNotificationMessageArrived：接收服务器发来的通知栏消息（消息到达客户端时触发，并且可以接收应用在前台时不弹出通知的通知消息）  
7、onCommandResult：接收客户端向服务器发送命令消息后返回的响应  
8、onReceiveRegisterResult：用来接受客户端向服务器发送注册命令消息后返回的响应
例如： 自定义的BroadcastReceiver  
``` java
public class XMPushReceiver extends PushMessageReceiver {
    private String mRegId;
    private String mTopic;
    private String mAlias;
    private String mAccount;
    private String mStartTime;
    private String mEndTime;

    @Override
    public void onReceivePassThroughMessage(Context context, MiPushMessage message) {
        //接收服务器推送的透传消息，消息封装在 MiPushMessage类中
        AppSpLog.i("onReceivePassThroughMessage is called. " + message.toString());
        if (!TextUtils.isEmpty(message.getTopic())) {
            mTopic = message.getTopic();
        } else if (!TextUtils.isEmpty(message.getAlias())) {
            mAlias = message.getAlias();
        } else if (!TextUtils.isEmpty(message.getUserAccount())) {
            mAccount = message.getUserAccount();
        }
    }

    @Override
    public void onNotificationMessageClicked(Context context, MiPushMessage message) {
        //接收服务器推送的通知消息，用户点击后触发，消息封装在 MiPushMessage类中
        AppSpLog.i("onNotificationMessageClicked is called. " + message.toString());

        if (!TextUtils.isEmpty(message.getTopic())) {
            mTopic = message.getTopic();
        } else if (!TextUtils.isEmpty(message.getAlias())) {
            mAlias = message.getAlias();
        } else if (!TextUtils.isEmpty(message.getUserAccount())) {
            mAccount = message.getUserAccount();
        }

        //todo 点击通知栏消息，发送广播给打开自定义页面

        Intent intent = new Intent();
        intent.setAction("com.anji.plus.ajpushlibrary.PushMessageReceiver");
        Bundle bundle = new Bundle();
        NotificationMessageModel notificationMessageModel = new NotificationMessageModel();
        notificationMessageModel.setTitle(message.getTitle());
        notificationMessageModel.setContent(message.getContent());
        JSONObject json = new JSONObject(message.getExtra());
        notificationMessageModel.setNotificationExtras(json.toString());
        notificationMessageModel.setMessageType(3);
        bundle.putSerializable(AppParam.HNOTIFICATIONMESSAGE, notificationMessageModel);
        intent.putExtras(bundle);
        intent.setComponent(new ComponentName(AppParam.packageName, AppParam.packageName + ".PushMessageReceiver"));
        context.sendBroadcast(intent);
    }

    @Override
    public void onNotificationMessageArrived(Context context, MiPushMessage message) {
        //接收服务器推送的通知消息，消息到达客户端时触发，还可以接受应用在前台时不弹出通知的通知消息，消息封装在 MiPushMessage类中
        AppSpLog.i("onNotificationMessageArrived is called. " + message.toString());

        if (!TextUtils.isEmpty(message.getTopic())) {
            mTopic = message.getTopic();
        } else if (!TextUtils.isEmpty(message.getAlias())) {
            mAlias = message.getAlias();
        } else if (!TextUtils.isEmpty(message.getUserAccount())) {
            mAccount = message.getUserAccount();
        }
    }

    @Override
    public void onCommandResult(Context context, MiPushCommandMessage message) {
        //获取给服务器发送命令的结果，结果封装在MiPushCommandMessage类中
        AppSpLog.i("onCommandResult is called. " + message.toString());
        String command = message.getCommand();
        List<String> arguments = message.getCommandArguments();
        String cmdArg1 = ((arguments != null && arguments.size() > 0) ? arguments.get(0) : null);
        String cmdArg2 = ((arguments != null && arguments.size() > 1) ? arguments.get(1) : null);
        if (MiPushClient.COMMAND_REGISTER.equals(command)) {
            if (message.getResultCode() == ErrorCode.SUCCESS) {
                mRegId = cmdArg1;
            }
        } else if (MiPushClient.COMMAND_SET_ALIAS.equals(command)) {
            if (message.getResultCode() == ErrorCode.SUCCESS) {
                mAlias = cmdArg1;
            }
        } else if (MiPushClient.COMMAND_UNSET_ALIAS.equals(command)) {
            if (message.getResultCode() == ErrorCode.SUCCESS) {
                mAlias = cmdArg1;
            }
        } else if (MiPushClient.COMMAND_SET_ACCOUNT.equals(command)) {
            if (message.getResultCode() == ErrorCode.SUCCESS) {
                mAccount = cmdArg1;
            }
        } else if (MiPushClient.COMMAND_UNSET_ACCOUNT.equals(command)) {
            if (message.getResultCode() == ErrorCode.SUCCESS) {
                mAccount = cmdArg1;
            }
        } else if (MiPushClient.COMMAND_SUBSCRIBE_TOPIC.equals(command)) {
            if (message.getResultCode() == ErrorCode.SUCCESS) {
                mTopic = cmdArg1;
            }
        } else if (MiPushClient.COMMAND_UNSUBSCRIBE_TOPIC.equals(command)) {
            if (message.getResultCode() == ErrorCode.SUCCESS) {
                mTopic = cmdArg1;
            }
        } else if (MiPushClient.COMMAND_SET_ACCEPT_TIME.equals(command)) {
            if (message.getResultCode() == ErrorCode.SUCCESS) {
                mStartTime = cmdArg1;
                mEndTime = cmdArg2;
            }
        }
    }

    @Override
    public void onReceiveRegisterResult(Context context, MiPushCommandMessage message) {
        //获取给服务器发送注册命令的结果，结果封装在MiPushCommandMessage类中
        AppSpLog.i("onReceiveRegisterResult is called. " + message.toString());
        String command = message.getCommand();
        List<String> arguments = message.getCommandArguments();
        String cmdArg1 = ((arguments != null && arguments.size() > 0) ? arguments.get(0) : null);
        String cmdArg2 = ((arguments != null && arguments.size() > 1) ? arguments.get(1) : null);
        if (MiPushClient.COMMAND_REGISTER.equals(command)) {
            if (message.getResultCode() == ErrorCode.SUCCESS) {
                mRegId = cmdArg1;
                //收到regId之后,可以设置alias、userAccount、topic，将获取的token传给服务器
                AppParam.pushToken = mRegId;
                AppSpConfig.getInstance().sendRegTokenToServer(new IAppSpCallback() {
                    @Override
                    public void pushInfo(AppSpModel<String> appSpModel) {

                    }

                    @Override
                    public void error(String code, String msg) {

                    }
                });
            }
        }

    }
}

```

### 注册推送服务

通过调用MiPushClient.registerPush来初始化小米推送服务。注册成功后，可以在自定义的onCommandResult和onReceiveRegisterResult中收到注册结果，其中的regId即是当前设备上当前app的唯一标示。可以将regId上传到自己的服务器，方便向其发消息 
``` java
    @Override
        public void onCreate() {
            super.onCreate();
            //初始化push推送服务
            if(shouldInit()) {
                MiPushClient.registerPush(this, APP_ID, APP_KEY);
            }

    private boolean shouldInit() {
            ActivityManager am = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE));
            List<RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
            String mainProcessName = getApplicationInfo().processName;
            int myPid = Process.myPid();
            for (RunningAppProcessInfo info : processInfos) {
                if (info.pid == myPid && mainProcessName.equals(info.processName)) {
                    return true;
                }
            }
            return false;
        }      
``` 
注意：  
（1）因为推送服务XMPushService在AndroidManifest.xml中设置为运行在另外一个进程，这导致本Application会被实例化两次，所以我们需要让应用的主进程初始化  
（2）在非MIUI平台下，如果targetSdkVersion>=23，需要动态申请电话和存储权限，请在申请权限后再调用注册接口，否则会注册失败。  

### 点击通知栏打开自定义页面 

1、在DemoMessageReceiver中的onNotificationMessageClicked（）方法中，添加如下代码  
``` java
    try {
                //打开自定义的Activity
                Intent i = new Intent(context, Main3Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("XMContent", message.getContent());
                i.putExtras(bundle);
                context.startActivity(i);
            } catch (Throwable throwable) {

            }
``` 
2、在自定义界面中接收数据信息
``` java  
    private void getXMIntentData(Intent intent) {
        TextView tv = new TextView(this);
        tv.setText("用户自定义打开的Activity_XM");
        if (null != intent) {
            Bundle bundle = getIntent().getExtras();
            String content = null;
            if (bundle != null) {
                content = bundle.getString("XMContent");
            }
            tv.setText("用户自定义打开的Activity_XM，Content : " + content);
        }
        addContentView(tv, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
    }
``` 

## 推送测试  

1、点击“创建推送”,如下图所示：  
![avatar](../../assets/xm5.png)   
2、添加推送消息内容，如下图所示：  
![avatar](../../assets/xm6.png)
