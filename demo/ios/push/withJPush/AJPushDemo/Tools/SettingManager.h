//
//  SettingManager.h
//  AJPushDemo
//
//  Created by Black on 2021/2/4.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettingManager : NSObject

+ (NSString *)getJPushKey;
+ (NSString *)getAppSpKey;
+ (NSString *)getAppSpSecretKey;
+ (NSString *)getAppSpUrl;

+ (void)setJPushKey:(NSString *)jpushKey;
+ (void)setAppSpKey:(NSString *)appSpKey;
+ (void)setAppSpUrl:(NSString *)appSpUrl;
+ (void)setAppSpSecretKey:(NSString *)secretKey;
@end

NS_ASSUME_NONNULL_END
