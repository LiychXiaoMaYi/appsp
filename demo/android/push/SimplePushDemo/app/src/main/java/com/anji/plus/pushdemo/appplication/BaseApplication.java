package com.anji.plus.pushdemo.appplication;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.anji.plus.ajpushlibrary.AppSpPushConfig;
import com.anji.plus.ajpushlibrary.AppSpPushLog;
import com.anji.plus.ajpushlibrary.AppSpPushConstant;
import com.anji.plus.ajpushlibrary.http.AppSpPushRequestUrl;
import com.anji.plus.pushdemo.SoundUtils;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * Appsp 推送
 * </p>
 */
public class BaseApplication extends Application {
    private Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        SoundUtils.init(this);
        mContext = getApplicationContext();
        initConfiguration();
        debugConfiguration();
    }

    ///从build.gradle读取配置
    private void initConfiguration() {
        try {
            ApplicationInfo appInfo = mContext.getPackageManager().getApplicationInfo(mContext.getPackageName(),
                    PackageManager.GET_META_DATA);
            AppSpPushConstant.packageName = mContext.getPackageName();
            AppSpPushRequestUrl.Host = AppSpPushConstant.appspHost = getMetaInfoByKey(appInfo, "appspHost");
            AppSpPushConstant.appspAppKey = getMetaInfoByKey(appInfo, "appspAppKey");
            AppSpPushConstant.appspSecretKey = getMetaInfoByKey(appInfo, "appspSecretKey");
            AppSpPushConstant.oppoAppKey = getMetaInfoByKey(appInfo, "oppoAppKey");
            AppSpPushConstant.oppoAppSecret = getMetaInfoByKey(appInfo, "oppoAppSecret");
            AppSpPushConstant.xmAppId = getMetaInfoByKey(appInfo, "xmAppId");
            AppSpPushConstant.xmAppKey = getMetaInfoByKey(appInfo, "xmAppKey");
        } catch (Exception e) {

        }
        AppSpPushConfig.getInstance().init(mContext, AppSpPushConstant.appspAppKey, AppSpPushConstant.appspSecretKey, AppSpPushRequestUrl.Host + AppSpPushRequestUrl.putPushInfo);
    }

    private String getMetaInfoByKey(ApplicationInfo appInfo, String key) {
        Object value = appInfo.metaData.get(key);
        String valueStr = "";
        if (value != null) {
            valueStr = value.toString().trim();
        }
        return valueStr;
    }

    private void debugConfiguration() {
        AppSpPushLog.d(" AppSpPushRequestUrl.Host " + AppSpPushRequestUrl.Host);
        AppSpPushLog.d(" AppSpPushConstant.packageName " + AppSpPushConstant.packageName);
        AppSpPushLog.d(" AppSpPushConstant.appspAppKey " + AppSpPushConstant.appspAppKey);
        AppSpPushLog.d(" AppSpPushConstant.appspSecretKey " + AppSpPushConstant.appspSecretKey);
        AppSpPushLog.d(" AppSpPushConstant.oppoAppKey " + AppSpPushConstant.oppoAppKey);
        AppSpPushLog.d(" AppSpPushConstant.oppoAppSecret " + AppSpPushConstant.oppoAppSecret);
        AppSpPushLog.d(" AppSpPushConstant.xmAppId " + AppSpPushConstant.xmAppId);
        AppSpPushLog.d(" AppSpPushConstant.xmAppKey " + AppSpPushConstant.xmAppKey);
    }
}
