package com.anji.plus.ajpushlibrary.base;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * Appsp-Push 支持的手机类型
 * </p>
 */
public class BrandType {
    public static final int HUAWEI = 0x01;
    public static final int XIAOMI = 0x02;
    public static final int OPPO = 0x03;
    public static final int VIVO = 0x04;
    public static final int OTHER = 0x05;
}
