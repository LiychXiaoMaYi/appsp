package com.anji.sp.exception;

/**
 * 演示模式异常
 * 
 * @author zhiyuan
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException() {
    }
}
