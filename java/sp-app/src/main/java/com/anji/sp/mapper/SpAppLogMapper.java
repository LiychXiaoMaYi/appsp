package com.anji.sp.mapper;

import com.anji.sp.model.po.SpAppLogPO;
import com.anji.sp.model.vo.SpAppLogVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Map;

/**
 * app请求log表
 *
 * @author Kean 2020-06-23
 */
public interface SpAppLogMapper extends BaseMapper<SpAppLogPO> {
    Long getDeviceIdCount(SpAppLogVO spAppLogVO);
    /**
     * 创建表 根据时间
     * @param map
     */
    void createTable(Map<String, String> map);

    /**
     * copy 表
     * @param map
     */
    void copyArchiveData(Map<String, String> map);

    /**
     * 清除小于某个时间的表
     * @param map
     */
    void deleteArchiveData(Map<String, String> map);

}