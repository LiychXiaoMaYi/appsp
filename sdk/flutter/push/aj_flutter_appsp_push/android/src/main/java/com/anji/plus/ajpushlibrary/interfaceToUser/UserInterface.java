package com.anji.plus.ajpushlibrary.interfaceToUser;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.anji.plus.ajpushlibrary.AppSpPushConfig;
import com.anji.plus.ajpushlibrary.http.AppSpPushCallBack;
import com.anji.plus.ajpushlibrary.http.AppSpPushHttpClient;
import com.anji.plus.ajpushlibrary.http.AppSpPushPostData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author : zhouyang01
 * e-mail : xxx@xx
 * phone  :
 * time   : 2021/03/17
 * desc   :
 */
public class UserInterface {
    /**
     * @param path          请求地址
     * @param deviceIds     设备id集
     * @param title         通知标题
     * @param content       通知内容
     * @param pushType      推送类型 1透传消息; 0普通消息（默认0）
     * @param extras        可选参数    推送透传消息内容
     * @param androidConfig 可选参数    Android其他配置例如：传声音{"sound":"xxx"}
     * @param iosConfig     可选参数      iOS其他配置例如：传声音{"sound":"xxx.caf"}
     */
    public static void pushBeachByDevicesId(
            Context context,
            String path,
            List<String> deviceIds,
            String title,
            String content,
            String pushType,
            Map<String, String> extras,
            Map<String, String> androidConfig,
            Map<String, String> iosConfig,
            MyCallBack myCallBack
    ) {
        if (TextUtils.isEmpty(path)) {
            Toast.makeText(context, "请求地址不能为空", Toast.LENGTH_LONG).show();
            return;
        }
        if (deviceIds == null && deviceIds.size() == 0) {
            Toast.makeText(context, "设备Id不能为空", Toast.LENGTH_LONG).show();
            return;
        }


        if (TextUtils.isEmpty(pushType)) {
            pushType = "0";
        }

        if (extras == null) {
            extras = new HashMap<>();
        }
        if (androidConfig == null) {
            androidConfig = new HashMap<>();
        }
        if (iosConfig == null) {
            iosConfig = new HashMap<>();
        }

        AppSpPushPostData appSpPushPostData = new AppSpPushPostData();
        try {
            appSpPushPostData.put("appKey", AppSpPushConfig.appKey);
            appSpPushPostData.put("secretKey", AppSpPushConfig.secretKey);
            appSpPushPostData.put("title", title);
            appSpPushPostData.put("content", content);
            appSpPushPostData.put("pushType", pushType);
            appSpPushPostData.put("deviceIds", deviceIds);
            appSpPushPostData.put("extras", extras);
            appSpPushPostData.put("androidConfig", androidConfig);
            appSpPushPostData.put("iosConfig", iosConfig);
        } catch (Exception e) {

        }
        AppSpPushHttpClient client = new AppSpPushHttpClient();
        client.request(path, appSpPushPostData, new AppSpPushCallBack() { //周阳修改
            @Override
            public void onSuccess(String data) {
                myCallBack.success(data);
            }

            @Override
            public void onError(String code, String msg) {
                myCallBack.error(code, msg);
            }
        });
    }

    /**
     * 向所有设备推送信息
     *
     * @param path 请求地址
     */
    public static void pushAll(String path, MyCallBack myCallBack) {
        AppSpPushPostData appSpPushPostData = new AppSpPushPostData();
        try {
            appSpPushPostData.put("appKey", AppSpPushConfig.appKey);
            appSpPushPostData.put("secretKey", AppSpPushConfig.secretKey);
            appSpPushPostData.put("title", "周sir在测试批量推送");
            appSpPushPostData.put("content", "i am content");
            appSpPushPostData.put("pushType", "0");
        } catch (Exception e) {

        }
        AppSpPushHttpClient client = new AppSpPushHttpClient();
        client.request(path, appSpPushPostData, new AppSpPushCallBack() { //周阳修改
            @Override
            public void onSuccess(String data) {
                myCallBack.success(data);
            }

            @Override
            public void onError(String code, String msg) {
                myCallBack.error(code, msg);
            }
        });
    }

    /**
     * 查询某部手机收到的 历史推送消失
     *
     * @param path  请求地址
     * @param msgId 从批量推送返回的消息id
     */
    public static void selectPushHistoryInfo(Context context, String path, String msgId, MyCallBack myCallBack) {
        if (TextUtils.isEmpty(path)) {
            Toast.makeText(context, "请求地址不能为空", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(msgId)) {
            Toast.makeText(context, "msgId不能为空", Toast.LENGTH_LONG).show();
            return;
        }

        AppSpPushPostData appSpPushPostData = new AppSpPushPostData();
        try {
            appSpPushPostData.put("appKey", AppSpPushConfig.appKey);
            appSpPushPostData.put("secretKey", AppSpPushConfig.secretKey);
            appSpPushPostData.put("msgId", msgId);
        } catch (Exception e) {

        }
        AppSpPushHttpClient client = new AppSpPushHttpClient();
        client.request(path, appSpPushPostData, new AppSpPushCallBack() {
            @Override
            public void onSuccess(String data) {
                myCallBack.success(data);
            }

            @Override
            public void onError(String code, String msg) {
                myCallBack.error(code, msg);
            }
        });
    }


    MyCallBack myCallBack;

    public interface MyCallBack {
        void success(String message);

        void error(String code, String message);
    }


}
