package com.anji.plus.ajpushlibrary.http;

public enum AppSpPushRequestMethod {
    POST,
    GET,
    DELETE,
    PUT
}
