#!/bin/bash

#判断node.js mvn是否存在
command -v npm >/dev/null 2>&1 || { echo >&2 "I require node.js v14.16.0+ but it's not installed.  Aborting."; sleep 5; exit 1; }
command -v mvn >/dev/null 2>&1 || { echo >&2 "I require maven 3.5 + but it's not installed.  Aborting."; sleep 5; exit 1; }

cd `dirname $0`
BuildDir=`pwd` #工程根目录
echo $BuildDir


echo "build web"
cd $BuildDir/web
npm install >/dev/null 2>&1
npm run build:prod >/dev/null 2>&1

echo "publish web to springboot src/main/resources/static"

mkdir -p $BuildDir/java/sp-app/src/main/resources/static
mv $BuildDir/web/dist/* $BuildDir/java/sp-app/src/main/resources/static/


echo "build springboot"
cd $BuildDir/java
mvn clean >/dev/null 2>&1
mvn package -Dmaven.test.skip=true -Dfile.encoding=UTF-8 >/dev/null 2>&1

echo "zip finish in build dir"
if [ ! -d "$BuildDir/build" ]; then
    mkdir $BuildDir/build
fi
mv $BuildDir/java/sp-app/target/sp-app-*.zip $BuildDir/build/
rm -rf $BuildDir/java/sp-app/src/main/resources/static/*;

